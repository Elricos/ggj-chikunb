return {
  version = "1.1",
  luaversion = "5.1",
  orientation = "orthogonal",
  width = 20,
  height = 15,
  tilewidth = 32,
  tileheight = 32,
  properties = {},
  tilesets = {
    {
      name = "roomBackground",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/roomBackground.png",
      imagewidth = 1024,
      imageheight = 1024,
      properties = {},
      tiles = {}
    },
    {
      name = "door1",
      firstgid = 1025,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/door/door1.png",
      imagewidth = 96,
      imageheight = 96,
      properties = {},
      tiles = {}
    },
    {
      name = "rug",
      firstgid = 1034,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/furniture/rug.png",
      imagewidth = 96,
      imageheight = 96,
      properties = {},
      tiles = {}
    },
    {
      name = "door2",
      firstgid = 1043,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/door/door2.png",
      imagewidth = 96,
      imageheight = 96,
      properties = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Tile Layer 1",
      x = 0,
      y = 0,
      width = 20,
      height = 15,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        557, 558, 559, 560, 561, 562, 563, 564, 565, 566, 567, 568, 569, 570, 571, 572, 573, 478, 479, 480,
        589, 590, 591, 592, 593, 594, 595, 596, 597, 598, 599, 600, 601, 602, 603, 604, 605, 510, 511, 512,
        621, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 542, 543, 544,
        653, 654, 655, 656, 657, 658, 659, 660, 661, 662, 663, 664, 665, 666, 667, 668, 669, 574, 575, 576,
        685, 686, 687, 688, 689, 690, 691, 692, 693, 694, 695, 696, 697, 698, 699, 700, 701, 606, 607, 608,
        717, 718, 719, 720, 721, 722, 723, 724, 725, 726, 727, 728, 729, 730, 731, 732, 733, 734, 735, 736,
        749, 750, 751, 752, 753, 754, 755, 756, 757, 758, 759, 760, 761, 762, 763, 764, 765, 766, 767, 768,
        781, 782, 783, 784, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 799, 800,
        813, 814, 815, 816, 817, 818, 819, 820, 821, 822, 823, 824, 825, 826, 827, 828, 829, 830, 831, 832,
        845, 846, 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859, 860, 861, 862, 863, 864,
        877, 878, 879, 880, 881, 882, 883, 884, 885, 886, 887, 888, 889, 890, 891, 892, 893, 894, 895, 896,
        909, 910, 911, 912, 913, 914, 915, 916, 917, 918, 919, 920, 921, 922, 923, 924, 925, 926, 927, 928,
        943, 943, 944, 945, 945, 946, 947, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, 960,
        975, 975, 976, 977, 977, 978, 979, 980, 981, 982, 983, 984, 985, 986, 987, 988, 989, 990, 991, 992,
        1007, 1007, 1008, 1009, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021, 1022, 1023, 1024
      }
    },
    {
      type = "tilelayer",
      name = "Tile Layer 2",
      x = 0,
      y = 0,
      width = 20,
      height = 15,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        1038, 1038, 1038, 1038, 1038, 1038, 1039, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1038, 1038, 1038, 1038, 1038, 1038, 1039, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1038, 1038, 1038, 1038, 1038, 1038, 1039, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1038, 1038, 1038, 1038, 1038, 1038, 1039, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1038, 1038, 1038, 1038, 1038, 1038, 1039, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        1041, 1041, 1041, 1041, 1041, 1041, 1042, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1043, 1044, 1045,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1046, 1047, 1048,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1049, 1050, 1051,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      name = "collisions",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 544,
          y = 0,
          width = 96,
          height = 480,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 384,
          width = 544,
          height = 96,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "entrances",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "top",
          type = "",
          shape = "rectangle",
          x = 32,
          y = 32,
          width = 512,
          height = 32,
          visible = true,
          properties = {}
        },
        {
          name = "left",
          type = "",
          shape = "rectangle",
          x = 32,
          y = 32,
          width = 32,
          height = 352,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "exits",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "top",
          type = "",
          shape = "rectangle",
          x = 32,
          y = 0,
          width = 512,
          height = 32,
          visible = true,
          properties = {
            ["toEntrance"] = "bottom",
            ["toMap"] = "roomOne2"
          }
        },
        {
          name = "left",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 32,
          width = 32,
          height = 352,
          visible = true,
          properties = {
            ["toEntrance"] = "right",
            ["toMap"] = "roomOne3"
          }
        }
      }
    },
    {
      type = "objectgroup",
      name = "moveLevel",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 512,
          y = 192,
          width = 32,
          height = 96,
          visible = true,
          properties = {
            ["hint"] = "This door is missing its handle. Maybe it was put away for safe keeping?",
            ["needsItem"] = "doorknob",
            ["nextLevel"] = "roomTwo1"
          }
        }
      }
    }
  }
}

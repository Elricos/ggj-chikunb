return {
  version = "1.1",
  luaversion = "5.1",
  orientation = "orthogonal",
  width = 20,
  height = 15,
  tilewidth = 32,
  tileheight = 32,
  properties = {},
  tilesets = {
    {
      name = "roomBackground",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/roomBackground.png",
      imagewidth = 1024,
      imageheight = 1024,
      properties = {},
      tiles = {}
    },
    {
      name = "rug",
      firstgid = 1025,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/furniture/rug.png",
      imagewidth = 96,
      imageheight = 96,
      properties = {},
      tiles = {}
    },
    {
      name = "dresser",
      firstgid = 1034,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/furniture/dresser.png",
      imagewidth = 64,
      imageheight = 32,
      properties = {},
      tiles = {}
    },
    {
      name = "dresser2",
      firstgid = 1036,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../gfx/furniture/dresser2.png",
      imagewidth = 64,
      imageheight = 32,
      properties = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Tile Layer 1",
      x = 0,
      y = 0,
      width = 20,
      height = 15,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        609, 610, 611, 548, 549, 550, 551, 552, 553, 554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564,
        641, 642, 643, 580, 581, 582, 583, 584, 585, 586, 587, 588, 589, 590, 591, 592, 593, 594, 595, 596,
        673, 674, 675, 612, 613, 614, 615, 616, 617, 618, 619, 620, 621, 622, 623, 624, 625, 626, 627, 628,
        705, 706, 707, 644, 645, 646, 647, 648, 649, 650, 651, 652, 653, 654, 655, 656, 657, 658, 659, 660,
        737, 738, 739, 676, 677, 678, 679, 680, 681, 682, 683, 684, 685, 686, 687, 688, 689, 690, 691, 692,
        769, 770, 771, 708, 709, 710, 711, 712, 713, 714, 715, 716, 717, 718, 719, 720, 721, 722, 723, 724,
        769, 770, 771, 740, 741, 742, 743, 744, 745, 746, 747, 748, 749, 750, 751, 752, 753, 754, 755, 756,
        769, 770, 771, 772, 773, 774, 775, 776, 777, 778, 779, 780, 781, 782, 783, 784, 785, 786, 787, 788,
        801, 802, 803, 804, 805, 806, 807, 808, 809, 810, 811, 812, 813, 814, 815, 816, 817, 818, 819, 820,
        833, 834, 835, 836, 837, 838, 839, 840, 841, 842, 843, 844, 845, 846, 847, 848, 849, 850, 851, 852,
        865, 866, 867, 868, 869, 870, 871, 872, 873, 874, 875, 876, 877, 878, 879, 880, 881, 882, 883, 884,
        897, 898, 899, 900, 901, 902, 903, 904, 905, 906, 907, 908, 909, 910, 911, 912, 913, 914, 915, 916,
        929, 930, 931, 932, 933, 933, 934, 935, 936, 937, 938, 939, 940, 941, 942, 943, 944, 945, 946, 947,
        961, 962, 963, 964, 965, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 979,
        993, 994, 995, 996, 997, 997, 998, 999, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011
      }
    },
    {
      type = "tilelayer",
      name = "Tile Layer 2",
      x = 0,
      y = 0,
      width = 20,
      height = 15,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1028, 1029, 1029, 1029, 1029, 1029, 1029, 1029,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1028, 1029, 1029, 1029, 1029, 1029, 1029, 1029,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1028, 1029, 1029, 1029, 1029, 1029, 1029, 1029,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1028, 1029, 1029, 1029, 1029, 1029, 1029, 1029,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1028, 1029, 1029, 1029, 1029, 1029, 1029, 1029,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1031, 1032, 1032, 1032, 1032, 1032, 1032, 1032,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 1036, 1037, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "objectgroup",
      name = "collisions",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 0,
          width = 96,
          height = 480,
          visible = true,
          properties = {}
        },
        {
          name = "",
          type = "",
          shape = "rectangle",
          x = 96,
          y = 384,
          width = 544,
          height = 96,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "exits",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "top",
          type = "",
          shape = "rectangle",
          x = 96,
          y = 0,
          width = 512,
          height = 32,
          visible = true,
          properties = {
            ["toEntrance"] = "bottom",
            ["toMap"] = "roomOne1"
          }
        },
        {
          name = "right",
          type = "",
          shape = "rectangle",
          x = 608,
          y = 32,
          width = 32,
          height = 352,
          visible = true,
          properties = {
            ["toEntrance"] = "left",
            ["toMap"] = "roomOne4"
          }
        }
      }
    },
    {
      type = "objectgroup",
      name = "entrances",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "top",
          type = "",
          shape = "rectangle",
          x = 96,
          y = 32,
          width = 512,
          height = 32,
          visible = true,
          properties = {}
        },
        {
          name = "right",
          type = "",
          shape = "rectangle",
          x = 576,
          y = 32,
          width = 32,
          height = 352,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "furniture",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          name = "dresser",
          type = "",
          shape = "rectangle",
          x = 160,
          y = 352,
          width = 64,
          height = 32,
          visible = true,
          properties = {
            ["furnType"] = "dresser",
            ["givesItem"] = "doorknob",
            ["hint"] = "Looks like the drawers are locked. Perhaps there is a key under something?",
            ["needsItem"] = "unlock"
          }
        }
      }
    }
  }
}

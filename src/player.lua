-- chikun :: 2015
-- Player script



player = {}
myInventory = {}

-- Initalise hint text
hint = ""
hintTimer = 0
hintSwitch = false
fadeSwitch = false
fade = 0

--player walk animation
pWalk = {
        gfx.player.playerWalk1,
        gfx.player.playerWalk2,
        gfx.player.playerWalk3,
        gfx.player.playerWalk4,
        gfx.player.playerWalk5,
        gfx.player.playerWalk6,
    }

--player idle animation
pIdle = {
    gfx.player.playerIdleLeft1,
    gfx.player.playerIdleLeft2
    }

player.facing = 1
player.soundtime = 0


-- Called when map loads
player.set = function(x, y, w, h)


    -- Set player position
    player.x = x ; player.y = y
    player.w = w ; player.h = h

    -- Set player image
    player.image = pWalk

    -- loop counter
    player.loop  = 0


end


bgtrax = {
    bgm.hltrskltr,
    bgm.trepidation,
    bgm.drkning,
    bgm.verydarkfinalstage,
    bgm.verydarkfinalstage,
    bgm.credits
}

bglvl = 1
bgm = bgtrax[bglvl]
bgm:setLooping(true)
cansound = true


g.setFont(fnt.splash)


-- Called using update phase
player.update = function(dt)

    player.moving = false

    --For the ending
    if fadeSwitch then
        fade = fade + 1
    end

    if fade >= 254 then
        fade = 255
        state.load(states.credits)
    end




    a.setVolume(0.5 * (optsMenu[1].val / 100))




    -- Press Escape at any point to quit
    if (k.isDown('escape')) then
        e.quit()
    end

    function love.keyreleased(key)
        if key == "e" then
            cansound = true
        end
    end

    function love.keypressed(key)
        --[[if key == "m" then
            state.load(states.menu)
            bgm:stop()
        end]]--

        if key == "i" then
            state.load(states.inventory)
        end

    end


    -- Figure out which direction the player will move horizontally
    local xMove = 0
    if (k.isDown('left'))  then
        xMove = -1
    end
    if (k.isDown('right')) then
        xMove = xMove + 1
    end

    -- direction player will move vertically
    local yMove = 0
    if (k.isDown('up'))  then
        yMove = -1
    end
    if (k.isDown('down')) then
        yMove = yMove + 1
    end



    -- checks if player moving
    if xMove ~= 0 or yMove ~= 0 then

        player.moving = true

    end

    -- if moving horizontaally it checks direction to face
    if xMove ~= 0 then

        player.facing = xMove * -1

    end


    -- speed of horizontal movement
    player.x = math.round(player.x + 160 * dt * xMove)

    -- collision detection
    while math.overlapTable(collisions, player) do

        player.x = math.round(player.x - xMove)

    end

    -- speed of vertical movement
    player.y = math.round(player.y + 160 * dt * yMove)

    -- collision detection
    while math.overlapTable(collisions, player) do

        player.y = math.round(player.y - yMove)

    end


    -- increments loop by dt(delta time)
    player.loop = player.loop + dt
    if hintSwitch then
        hintTimer = hintTimer + dt
    end
    if hintTimer >= 1.5 then
        hintTimer = 0
        hint = ""
        hintSwitch = false
    end


    -- sets speed of animation
    if player.loop >=0.8 then
        player.loop = player.loop - 0.8
    end


    -- when moving uses pWalk table otherwise pIdle table
    if player.moving then

        player.image = pWalk

    else

        player.image = pIdle

    end

    -- when moving plays footstep sounds
    if player.moving then
        player.soundtime = player.soundtime + dt
    else
        player.soundtime = 0
    end

    if player.soundtime >= 0.4 then
        a.play(sfx.footstep)
        player.soundtime = 0
    end


    --check collision for furniture and then check if 'e' is pressed
    for key, value in ipairs(furniture) do

        if math.overlap(value, player) then
            -- when needed to use inventory item to interact with furniture that requires it
            if player.usingItem == value.properties.needsItem and player.usingItem ~= nil then
                furnType = value.properties.type -- grab the furniture type so we know what sound it makes
                if cansound then -- check that we haven't already played the sound for this interaction
                    a.setVolume(0.5 * (optsMenu[1].val / 100))
                    a.play(sfx.opendrawer)
                end
                cansound = false -- Now we know that we have already played the sound for this interaction
                value.properties.used = true
                given = value.properties.givesItem
                items[given].has = true
                hint = "You found a "..items[given].name
                hintSwitch = true
                items[value.properties.needsItem].has = false



            elseif (k.isDown('e')) and (value.properties.used ~= true) and (value.properties.needsItem == nil) then

                -- interact
                furnType = value.properties.type -- grab the furniture type so we know what sound it makes
                if cansound then -- check that we haven't already played the sound for this interaction
                    a.setVolume(0.5 * (optsMenu[1].val / 100))
                    a.play(sfx.opendrawer)
                end
                cansound = false -- Now we know that we have already played the sound for this interaction
                function love.keyreleased(key)
                    if key == "e" then
                        value.properties.used = true
                    end
                end
                given = value.properties.givesItem
                items[given].has = true
                hint = "You found a "..items[given].name
                hintSwitch = true

                -- When ready, un-comment this code to have sound effects for opening drawers and chests:

                --[[ if furnType == "drawer" then
                    a.play(sfx.opendrawer)
                elseif furnType == "chest" then
                    a.play(sfx.openchest)
                end ]]--

                -- For now, though, we only need drawers, so I'll hard-code that in and we can take it out later:

            elseif (k.isDown('e')) and (value.properties.used == true) then -- cansound checks that we can play sounds
                if cansound then
                    a.play(sfx.itemusefail)
                    cansound = false
                end
                hint = "This has already been used"
                hintSwitch = true

            elseif value.properties.used ~= true and value.properties.needsItem ~= nil then
                if items[value.properties.needsItem].has == false then
                    if k.isDown('e') then
                        hint = value.properties.hint
                        hintSwitch = true
                        if cansound then -- check that we haven't already played the sound for this interaction
                            a.setVolume(0.5 * (optsMenu[1].val / 100))
                            a.play(sfx.itemusefail)
                        end
                    end
                elseif items[value.properties.needsItem].has == true then
                    if k.isDown('e') then
                        hint = "Look in your inventory!"
                        hintSwitch = true
                        if cansound then -- check that we haven't already played the sound for this interaction
                            a.setVolume(0.5 * (optsMenu[1].val / 100))
                            a.play(sfx.itemusefail)
                        end
                    end
                end
            end
        end
    end

    -- checks for collision with each exit in the current map
    for key, value in ipairs(exits) do

        if math.overlap(value, player) and maps[value.properties.toMap] ~= nil then

            mapChange(maps[value.properties.toMap], value.properties.toEntrance) -- uses properties to find which map to move to

        end
    end


    -- checks to move to next level
    for key, value in ipairs(moveLevel) do


        if math.overlap(value, player) then


            -- press e to open the f**king door
            if player.usingItem == value.properties.needsItem and player.usingItem ~= nil then

                    -- print("KNOCK KNOCK MOTHERFUCKER") -- This line is for debugging purposes
                    if cansound then -- Checking that we haven't already played a sound for this interaction
                        a.play(sfx.opendoor) -- Play the sound of a door opening
                    end
                    items[value.properties.needsItem].has = false
                    cansound = false -- Now we know we have played the sound for this interaction, and we can't do it again until we let go of the key.
                    mapChange(maps[value.properties.nextLevel]) -- Change maps. Duh.
                    bgm:stop()
                    if value.properties.nextLevel == "roomEnd" then
                        fadeSwitch = true
                    end
                    bglvl = bglvl + 1
                    bgm = bgtrax[bglvl]
                    bgm:play()

            elseif items[value.properties.needsItem].has then
                if k.isDown('e') then
                    hint = "Look in your inventory!"
                    hintSwitch = true
                    if cansound then -- check that we haven't already played the sound for this interaction
                        a.setVolume(0.5 * (optsMenu[1].val / 100))
                        a.play(sfx.itemusefail)
                    end
                end

            elseif items[value.properties.needsItem].has == false then
                if hint ~= nil then
                    hint = value.properties.hint
                    hintSwitch = true
                    if k.isDown('e') and cansound then
                        a.play(sfx.itemusefail)
                        cansound = false
                    end
                end

               -- mapChange(maps[value.properties.nextMap])
            end

        end
    end
    player.usingItem = ""
end



-- Called using draw phase
player.draw = function()


    -- Draw player
    g.draw(player.image[math.floor(player.loop * #player.image) + 1], player.x + player.w / 2, player.y - 32, 0, player.facing, 1, player.w / 2, 0)

    g.setColor(0, 0, 0)
    g.printf(hint, 1, w.getHeight() - 79, 2560, 'center', 0, 0.25)
    g.setColor(255,255,255)
    g.printf(hint, 0, w.getHeight() - 80, 2560, 'center', 0, 0.25)

    -- For the ending
    g.setColor(0, 0, 0, fade)
    g.rectangle("fill", 0, 0, w.getWidth(), w.getHeight())

end




-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local inventoryState = { }

--blank = gfx.misc.blank
inv = gfx.misc.inventory
blank = gfx.misc.blank

    items =
    {
            ["hammer"] = {
                pic = gfx.misc.hammer,
                slot = 1,
                width = 32,
                height = 32,
                x = 13,
                y = 15,
                has = false,
                name = "hammer"
            },
            ["unlock"] = {
                pic = gfx.misc.key,
                slot = 2,
                width = 32,
                height = 32,
                x = 49,
                y = 15,
                has = false,
                name = "key"
            },
            ["plank"] = {
                pic = gfx.misc.planks,
                slot = 3,
                width = 32,
                height = 32,
                x = 85,
                y = 15,
                has = false,
                name = "plank"
            },
            ["rope"] = {
                pic = gfx.misc.rope,
                slot = 4,
                width = 32,
                height = 32,
                x = 13,
                y = 51,
                has = false,
                name = "rope"
            },
            ["screw"] = {
                pic = gfx.misc.screwdriver,
                slot = 5,
                width = 32,
                height = 32,
                x = 49,
                y = 51,
                has = false,
                name = "screwdriver"
            },
            ["torch"] = {
                pic = gfx.misc.torch,
                slot = 6,
                width = 32,
                height = 32,
                x = 85,
                y = 51,
                has = false,
                name = "torch"
            },
            ["battery"] = {
                pic = gfx.misc.battery,
                slot = 7,
                width = 32,
                height = 32,
                x = 13,
                y = 87,
                has = false,
                name = "battery"
            },
            ["doorknob"] = {
                pic = gfx.misc.doorknob,
                slot = 8,
                width = 32,
                height = 32,
                x = 49,
                y = 87,
                has = false,
                name = "doorknob"
            },
            ["knife"] = {
                pic = gfx.misc.knife,
                slot = 9,
                width = 32,
                height = 32,
                x = 85,
                y = 87,
                has = false,
                name = "knife"
            },
    }


    --local slots = 10
    --inventoryBag = {hammer, blank, blank, blank, blank, blank, blank, blank, blank, blank}

-- On state create
function inventoryState:create()


    inventory = {
        x = (w.getWidth()/2) - (130/2), --
        y = (w.getHeight()/2) - (170/2),
        w = 130,
        h = 170
    }
    hint = "Press the corresponding number key to use an item."


end



-- On state update
function inventoryState:update(dt)

    --[[ for key, value in ipairs(items) do
            if value.has then
                g.draw(value.pic, (inventory.x + value.x), (inventory.y + value.y))
            end
    end ]]--


            function love.keypressed(key)
                if key == "i" then
                    hint = ""
                    state.current = states.play

                elseif key == "escape" then
                    e.quit()
                else
                    -- Check if item exists, then give it to player and close inventory
                    if items[checkInput(key)] ~= nil then
                        player.usingItem = checkInput(key)
                        -- items[checkInput(key)].has = false
                        hint = ""
                        state.current = states.play
                    end
                end
            end



end

function checkInput(key)
    keymap = {
        ["1"] = "hammer",
        ["2"] = "unlock",
        ["3"] = "plank",
        ["4"] = "rope",
        ["5"] = "screw",
        ["6"] = "torch",
        ["7"] = "battery",
        ["8"] = "doorknob",
        ["9"] = "knife",
        ["0"] = ""
    }
    if keymap[key] ~= nil then
        return keymap[key]
    else
        return nil
    end
end


function useItem(item)
    if item ~= nil then
        print("Selected Item in Slot: "..item.slot)
    else
        print("Item does not exist!")
    end
end


-- On state draw
function inventoryState:draw()

    -- draws the Inventory
    g.draw(inv, inventory.x, inventory.y)

    for key, value in pairs(items) do
            if value.has then
                g.draw(value.pic, (inventory.x + value.x), (inventory.y + value.y))
            end
    end

    g.printf(hint, 1, w.getHeight() - 79, 2560, 'center', 0, 0.25)

end




-- On state kill
function inventoryState:kill()

    inventory = {}

end


function getItem(item)

    inventory[#inventory] = item

end




-- Transfer data to state loading script
return inventoryState

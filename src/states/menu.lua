-- chikun :: 2014
-- Menu state


-- Temporary state, removed at end of script
local menuState = { }


-- On state create
function menuState:create()


    -- Set background colour to black
    g.setBackgroundColor(0, 0, 0)


    -- Initalise menu options
    mainmenu = {
        {id     =   "start",
        label   =   "START GAME",
        yPos    =   w.getHeight()/4
        },
        {id     =   "opts",
        label   =   "OPTIONS",
        yPos    =   w.getHeight()/2
        },
        {id     =   "quit",
        label   =   "QUIT GAME",
        yPos    =   w.getHeight()/4
        }
    }
    sel = 1
end

function menuChosen(id)
    if id == "start" then
        state.load(states.controls)
    elseif id == "opts" then
        state.load(states.options)
    elseif id == "quit" then
        e.quit()
    end
end


-- On state update
function menuState:update(dt)


    if sel > 3 then
        sel = 1
    elseif sel < 1 then
        sel = 3
    end

    a.setVolume(0.5 * (optsMenu[1].val / 100))

    function love.keypressed(key)
        if key == "down" then
            sel = sel + 1
        end

        if key == "up" then
            sel = sel - 1
        end

        if key == "return" then
            menuChosen(mainmenu[sel].id)
        end

        if key == "escape" then
            e.quit()
        end
    end
end


-- On state draw
function menuState:draw()


    -- Set font
    g.setFont(fnt.splash)

    -- Draw menu
    for key, value in ipairs(mainmenu) do
        if mainmenu[sel] == value then
            g.setColor(255, 255, 255)
        else
            g.setColor(128, 128, 128)
        end
        g.printf(value.label, 160, (120 * key) - (fnt.splash:getHeight() / 2), 640, 'center', 0, 0.5)
    end

end



-- On state kill
function menuState:kill()

end


-- Transfer data to state loading script
return menuState

-- chikun :: 2014
-- Controls state


-- Temporary state, removed at end of script
local controlsState = { }


-- On state create
function controlsState:create()


    -- Set background colour to black
    g.setBackgroundColor(0, 0, 0)

    -- Table of text
    text = {
        {str    =   "ARROW KEYS TO MOVE",
        pos     =   w.getHeight()/6
        },
        {str    =   "E TO INTERACT",
        pos     =   w.getHeight()/3
        },
        {str    =   "I FOR INVENTORY",
        pos     =   w.getHeight()/2
        },
        {str    =   "ESC TO EXIT",
        pos     =   2*w.getHeight()/3
        },
        {str    =   "RETURN/ENTER TO BEGIN",
        pos     =   5*w.getHeight()/6
        }
    }


end


-- On state update
function controlsState:update(dt)

    a.setVolume(0.5 * (optsMenu[1].val / 100))

    function love.keypressed(key)
        if key == "return" then
            menubgm:stop()
            state.load(states.play)
            bgm:play()
        end

        if key == "escape" then
            e.quit()
        end
    end


end


-- On state draw
function controlsState:draw()

        g.setFont(fnt.splash)
        g.setColor(255, 255, 255, 255)

        for key, value in ipairs(text) do
            g.printf(value.str, 0, value.pos - (fnt.splash:getHeight() / 2), 1280, 'center', 0, 0.5)
        end

end


-- On state kill
function controlsState:kill()

end


-- Transfer data to state loading script
return controlsState

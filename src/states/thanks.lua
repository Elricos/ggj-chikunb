-- chikun :: 2014
-- Splash state


-- Temporary state, removed at end of script
local thanksState = { }


-- On state create
function thanksState:create()


    -- Set background colour to black
    g.setBackgroundColor(0, 0, 0)

    -- Set off timer
    canQuit = false


end


-- On state update
function thanksState:update(dt)


    function love.keyreleased (key)
        if key == "return" then
            canQuit = true
        end
    end

    if k.isDown('return') and canQuit then
        menubgm:play()
        state.load(states.menu)
    end


end


-- On state draw
function thanksState:draw()


    -- Set font
    g.setFont(fnt.splash)

    -- Draw logo with formatting
    g.printf("THANKS FOR PLAYING!", 320, 0, 0, 'center')


end


-- On state kill
function thanksState:kill()


    -- Kill all variables
    timer = nil
    soundPlayed = nil


end


-- Transfer data to state loading script
return thanksState

-- chikun :: 2014
-- Controls state


-- Temporary state, removed at end of script
local creditsState = { }


-- On state create
function creditsState:create()


    -- Set background colour to black
    g.setBackgroundColor(0, 0, 0)

    -- Table of text
    text = {
        {str    =   "SCARNON",
        pos     =   w.getHeight()/7
        },
        {str    =   "A chikun production",
        pos     =   2*w.getHeight()/7
        },
        {str    =   "Keiran Dawson - Lead Programmer & Software Design",
        pos     =   3*w.getHeight()/7
        },
        {str    =   "Mathew Dwyer - Level Design, Art & Graphics Resources",
        pos     =   4*w.getHeight()/7
        },
        {str    =   "Chris Alderton - Sound Design, Music, Coding",
        pos     =   5*w.getHeight()/7
        },
        {str    =   "...and a big thanks to organisers, judges, and all involved with GGJ",
        pos     =   6*w.getHeight()/7
        }
    }


end


-- On state update
function creditsState:update(dt)

    a.setVolume(0.5 * (optsMenu[1].val / 100))

    function love.keypressed(key)
        if key == "return" then
            menubgm:stop()
            state.load(states.thanks)
            bgm:play()
        end

        if key == "escape" then
            e.quit()
        end
    end


end


-- On state draw
function creditsState:draw()

        g.setFont(fnt.splash)
        g.setColor(255, 255, 255, 255)

        for key, value in ipairs(text) do
            g.printf(value.str, 150, value.pos - (fnt.splash:getHeight() / 2), 1500, 'center', 0, 0.25)
        end

end


-- On state kill
function creditsState:kill()

end


-- Transfer data to state loading script
return creditsState

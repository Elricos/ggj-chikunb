-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local newState = { }
 --progression of maps
level = {
        maps.roomOne1,
        maps.roomTwo1
    }
counter = 1


-- On state create
function newState:create()

    mapChange(level[counter])
    counter = counter + 1

end


-- On state update
function newState:update(dt)

    player.update(dt)

end


-- On state draw
function newState:draw()


    g.rectangle("fill", 0, 520, 640, 40)

    map.draw()

    player.draw()

end


-- On state kill
function newState:kill()

end


-- Transfer data to state loading script
return newState

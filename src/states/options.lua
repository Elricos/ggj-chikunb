-- chikun :: 2014
-- Menu state


-- Temporary state, removed at end of script
local optionsState = { }


-- On state create
function optionsState:create()

    -- Set background colour to black
    g.setBackgroundColor(0, 0, 0)


    -- Initalise menu options
    optsMenu = {
        {id     =   "vol",
        label   =   "VOLUME: ",
        val     =   100,
        yPos    =   w.getHeight()/4
        },
        {id     =   "back",
        label   =   "BACK",
        yPos    =   3*w.getHeight()/4
        }
    }

    sel = 1
    --[[a.setVolume(0.5)
    bgm.title:setLooping(true)
    bgm.title:play()]]--
end

function optionsChosen(id)
    if id == "back" then
        state.load(states.menu)
    end
end


-- On state update
function optionsState:update(dt)

    if sel > 2 then
        sel = 1
    elseif sel < 1 then
        sel = 2
    end

    if optsMenu[1].val < 0 then
        optsMenu[1].val = 0
    elseif optsMenu[1].val > 100 then
        optsMenu[1].val = 100
    end

    function love.keypressed(key)
        if key == "down" then
            sel = sel + 1
        end

        if key == "up" then
            sel = sel - 1
        end

        if key == "left" then
            if sel == 1 then
                optsMenu[1].val = optsMenu[1].val - 10
            end
        end

        if key == "right" then
            if sel == 1 then
                optsMenu[1].val = optsMenu[1].val + 10
            end
        end

        if key == "return" then
            if sel == 2 then
                optionsChosen(optsMenu[sel].id)
            end
        end

        if key == "escape" then
            e.quit()
        end
    end
end


-- On state draw
function optionsState:draw()


    -- Set font
    g.setFont(fnt.splash)

    -- Draw menu
    for key, value in ipairs(optsMenu) do
        if optsMenu[sel] == value then
            g.setColor(255, 255, 255)
        else
            g.setColor(128, 128, 128)
        end
        if value.val ~= nil then
            g.printf(value.label..value.val, 160, (120 * key) - (fnt.splash:getHeight() / 2), 640, 'center', 0, 0.5)
        else
            g.printf(value.label, 160, (120 * key) - (fnt.splash:getHeight() / 2), 640, 'center', 0, 0.5)
        end
    end

end



-- On state kill
function optionsState:kill()

end


-- Transfer data to state loading script
return optionsState

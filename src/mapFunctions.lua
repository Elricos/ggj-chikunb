function resetMaps()

    maps = deepcopy(mapsBackup)

end


-- goes to new map
function mapChange(newMap, spot)

    map.current = newMap

    -- makes empty tables
    collisions = { }
    exits = { }
    entrances = { }
    important = { }
    furniture = { }
    moveLevel = { }



    -- for each key(element) it finds the layers
    for key, layer in ipairs(newMap.layers) do

        if layer.name == "important" then

            -- for each key it finds the objects
            for key, object in ipairs(layer.objects) do

                -- if player is not entering room from another place
                if object.name == "playerStart" and not spot then

                    player.set(object.x, object.y, 32, 32)

                end
            end


        elseif layer.name == "entrances" then

            for key, object in ipairs(layer.objects) do

                -- checks if the entrance is hotizontal or vertical and makes player enter and the same alignment
                if object.name == (spot or "") then

                    if spot == "left" or spot == "right" then

                        player.set(object.x, player.y, 32, 32)

                    else

                        player.set(player.x, object.y, 32, 32)

                    end

                end

            end

        -- load furniture
        elseif layer.name == "furniture" then

            furniture = layer.objects

        -- loads exits
        elseif layer.name == "exits" then

            exits = layer.objects


        -- loads collision layers
        elseif layer.name == "collisions" then

            collisions = layer.objects

        elseif layer.name == "moveLevel" then

            moveLevel = layer.objects

        end
    end
end

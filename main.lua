-- chikun :: 2015
-- A tutorial covering the specific chikun LÖVE libraries



--[[
    Here we will load the chikun specific libaries
  ]]
require "lib/bindings"      -- Very important that you read this file
require "lib/maths"         -- Load maths functions
require "lib/misc"          -- Load miscellaneous functions

--[[
    The recursive loading functions pretty much convert / to . and
    make file extensions void.
    eg. gfx/player.png becomes gfx.player,
        sfx/enemies/die becomes sfx.enemies.die
  ]]
require "lib/load/fonts"    -- Load fonts into fnt table [not recursive]
require "lib/load/gfx"      -- Load png graphics from gfx folder to gfx table
require "lib/load/maps"     -- Load all maps from map folder to maps table
require "lib/load/snd"      -- Load ogg sounds from bgm and sfx to bgm and sfx
require "lib/load/states"   -- Load states from src/states to states table

require "lib/stateManager"  -- Load state manager
require "lib/mapTool"       -- Load mapTool :: don't try reading this

require "src/mapFunctions"
require "src/player"        -- Load player object and functions



-- Performed at game startup
function love.load()


    mapsBackup = deepcopy(maps)

    -- Initalise menu options
    optsMenu = {
        {id     =   "sfx",
        label   =   "SFX: ",
        val     =   100,
        yPos    =   120
        },
        {id     =   "back",
        label   =   "BACK",
        yPos    =   360
        }
    }

    -- Initialise menu BGM
    menubgm = a.newSource('bgm/title.ogg')
    a.setVolume(0.5)
    menubgm:setLooping(true)

    -- Set current state to splash state
    state.load(states.splash)


end



-- Performed on game update
function love.update(dt)


    --[[
        Limit dt to 1/15 and induce lag if below 15 FPS.
        This is to save calculations.
      ]]
    dt = math.min(dt, 1/15)


    a.setVolume(0.5 * (optsMenu[1].val / 100))


    -- Update current state
    state.current:update(dt)


end



-- Performed on game draw
function love.draw()


    -- Set drawing colour to white [default]
    g.setColor(255, 255, 255)


    -- Draw current state
    state.current:draw()


end
